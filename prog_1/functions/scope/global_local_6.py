# Use of global: we bind with the global variable, and not with the local variable in
# outer scope

# Good reason do do this. In general, control change through return

def outer():
    def inner():
        global a_variable
        a_variable = 7
        print(a_variable)

    a_variable = 2
    print(a_variable)
    inner()
    print(a_variable)


a_variable = 10
outer()
print(a_variable)

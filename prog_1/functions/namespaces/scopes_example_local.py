A_CONSTANT = 3

def outer():
    def inner():
        a_variable = 7
        print(a_variable)

    another_variable = 2
    print(another_variable)
    inner()


outer()



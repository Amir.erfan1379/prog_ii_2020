import re

# Tag, attribute, value

text = """
<div>
    <label for="fname">First name:</label>
    <input id="fname" type="text">
</div>
<div>
    <label for="lname">Last name:</label>
    <input id="lname" type="text">
</div>
"""

# List of tags
re.findall(r'<.*?>', text)

# List of tags with id
re.findall(r'<.*?id.*?>', text)

# Get ids
mo3 = re.findall(r'<.*?id="(.*)" .*?>', text)

# Get tuples of ids and types
mo4 = re.findall(r'<.*?id="(.*)" type="(.*)".*?>', text)
# Modified from https://www.geeksforgeeks.org/protected-variable-in-python/
# program to illustrate protected
# data members in a class


# super class
class Shape:
    # A private class attribute, do not access from outside class
    __instance_count = 0

    # constructor
    def __init__(self, length, breadth):
        Shape.__instance_count += 1
        self._length = length
        self._breadth = breadth

    # public member function
    def display_sides(self):
        # accessing protected data members
        print("Length: ", self._length)
        print("Breadth: ", self._breadth)

    @classmethod
    def display_instance_count(cls):
        print(cls.__instance_count)


# derived class
class Rectangle(Shape):

    # constructor
    def __init__(self, length, breadth):
        # Calling the constructor of
        # Super class
        super().__init__(length, breadth)

    # public member function
    def calculate_area(self):
        # accessing protected data members of super class
        print("Area: ", self._length * self._breadth)


# derived class
obj = Rectangle(80, 50)

# calling derived member
# functions of the class
obj.display_sides()

# calling public member
# functions of the class
obj.calculate_area()

Shape.display_instance_count()
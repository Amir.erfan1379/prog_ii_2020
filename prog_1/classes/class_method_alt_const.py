class User:
    def __init__(self, name: str, user_id: str):
        self.name = name
        self.user_id = user_id

    @classmethod
    def from_dict(cls, dictionary: dict):
        return cls(name=dictionary.get('name'),
                   user_id=dictionary.get('user_id'))


# Regular constructor
user_1 = User("Bob", "123")
# Alternative constructor
user_2 = User.from_dict({'name': 'Frank', 'user_id': '345'})
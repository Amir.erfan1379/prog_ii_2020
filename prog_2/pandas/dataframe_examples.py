import pandas as pd


df = pd.DataFrame.from_dict(
    {"A": [1, 2, 3], "B": [4, 5, 6]},
    orient="index",
    columns=["one", "two", "three"],
)

#    one  two  three
# A    1    2      3
# B    4    5      6


print(df["one"])
df["three"] = df["one"] * df["two"]
df["flag"] = df["one"] > 2

#    one  two  three   flag
# A    1    2      2  False
# B    4    5     20   True


iris = pd.read_csv("prog_2/pandas/iris.csv", index_col=0)
iris.assign(sepal_ratio=iris["Sepal.Width"] / iris["Sepal.Length"]).head()
iris.assign(sepal_ratio=lambda x: (x["Sepal.Width"] / x["Sepal.Length"])).head()

#    Sepal.Length  Sepal.Width  Petal.Length  Petal.Width Species  sepal_ratio
# 1           5.1          3.5           1.4          0.2  setosa     0.686275
# 2           4.9          3.0           1.4          0.2  setosa     0.612245
# 3           4.7          3.2           1.3          0.2  setosa     0.680851
# 4           4.6          3.1           1.5          0.2  setosa     0.673913
# 5           5.0          3.6           1.4          0.2  setosa     0.720000

df = pd.DataFrame.from_dict(
    {"A": [1, 2, 3], "B": [4, 5, 6]},
    orient="index",
    columns=["one", "two", "three"],
)

#    one  two  three
# A    1    2      3
# B    4    5      6

df.loc["B"]

# one      4
# two      5
# three    6
# Name: B, dtype: int64

df.iloc[0]

# one      1
# two      2
# three    3
# Name: A, dtype: int64

df[0:2]

#    one  two  three
# A    1    2      3
# B    4    5      6
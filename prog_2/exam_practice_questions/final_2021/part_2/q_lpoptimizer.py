import numpy as np
from scipy.optimize import linprog

c = -np.array([350.0, 300.0])
A_ub = np.array([[1.0, 1.0],
                [18.0, 12.0],
                 [6.0, 8.0]])
b_ub = np.array([200.0, 3132.0, 1440.0])

x0_bounds = (0, None)
x1_bounds = (0, None)

bounds = [x0_bounds, x1_bounds]

result = linprog(c, A_ub=A_ub, b_ub=b_ub, bounds=bounds, method='interior-point')

print(result)

"""A popular use for descriptors is managing access to instance data. The descriptor is assigned
to a public attribute in the class dictionary while the actual data is stored as a private
attribute in the instance dictionary. The descriptor’s __get__() and __set__() methods are
triggered when the public attribute is accessed. """

# https://docs.python.org/3/howto/descriptor.html


import logging

logging.basicConfig(level=logging.INFO)


class LoggedAgeAccess:

    def __get__(self, instanace, owner=None):
        # noinspection PyProtectedMember
        value = instanace._age
        logging.info('Accessing %r giving %r', 'age', value)
        return value

    def __set__(self, instance, value):
        logging.info('Updating %r to %r', 'age', value)
        instance._age = value


class Person:
    age = LoggedAgeAccess()     # Descriptor

    def __init__(self, name, age):
        self.name = name        # Regular instance attribute
        self.age = age          # Calls the descriptor

    def birthday(self):
        self.age += 1


mary = Person('Mary M', 30)     # The initial age update is logged
dave = Person('David D', 40)

print(vars(mary))               # The actual data is in a private attribute
print(vars(dave))

print(mary.age)                 # Access the data and log the lookup

mary.birthday()                 # Updates are logged as well
print(dave.name)                # Regular attribute lookup isn't logged
print(dave.age)                 # Only the managed attribute is logged

# https://docs.python.org/3/library/sqlite3.html
# https://docs.python.org/3/library/sqlite3.html#sqlite3.Cursor.fetchone


import sqlite3

conn = sqlite3.connect('entertainment.db')

name = 'director'
table = 'Movies'
key = 'title'
sql_query = f'SELECT {name} FROM {table} WHERE {key}=?;'

# The sqlite3.Cursor class is an instance using which you can invoke methods that execute SQLite
# statements, fetch data from the result sets of the queries. You can create Cursor object using
# the cursor() method of the Connection object/class.

# cursor = conn.cursor()
# cursor.execute()
# or directly conn.executute()

result = conn.execute(sql_query, ['Jaws']).fetchone()

type(result)

sql_query_2 = "SELECT director FROM Movies WHERE title='Jaws';"

result_2 = conn.execute(sql_query_2).fetchone()

print(result_2[0])

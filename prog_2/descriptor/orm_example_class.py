# https://docs.python.org/3/library/sqlite3.html
# https://docs.python.org/3/library/sqlite3.html#sqlite3.Cursor.fetchone


import sqlite3

conn = sqlite3.connect('entertainment.db')


class Field:

    def __set_name__(self, owner, name):
        self.fetch = f'SELECT {name} FROM {owner.table} WHERE {owner.key}=?;'
        self.store = f'UPDATE {owner.table} SET {name}=? WHERE {owner.key}=?;'

    def __get__(self, instance, owner=None):
        return conn.execute(self.fetch, [instance.key]).fetchone()[0]

    def __set__(self, instance, value):
        conn.execute(self.store, [value, instance.key])
        conn.commit()


class Movie:
    table = 'Movies'  # Table name
    key = 'title'  # Primary key
    director = Field()
    year = Field()

    def __init__(self, key):
        self.key = key


class Song:
    table = 'Music'
    key = 'title'
    artist = Field()
    year = Field()
    genre = Field()

    def __init__(self, key):
        self.key = key


my_favorite_movie = Movie('Star Wars')
my_favorite_movies_director = my_favorite_movie.director
print(my_favorite_movies_director)

jaws = Movie('Jaws')
f'Released in {jaws.year} by {jaws.director}'

print(Song('Country Roads').artist)

Movie('Star Wars').director = 'J.J. Abrams'
print(Movie('Star Wars').director)

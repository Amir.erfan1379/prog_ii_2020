from collections import defaultdict

e = defaultdict(lambda: 'not_grade')
e.update(john='A', mary='A+')
print(e['john'])
print(e['peter'])


def constant_factory(value):
    return lambda: value


grades = defaultdict(constant_factory('<missing>'))
grades.update(john='A', mary='A+')


s = [('yellow', 1), ('blue', 2), ('yellow', 3), ('blue', 4), ('red', 1)]
d = defaultdict(list)
for k, v in s:
    d[k].append(v)

sorted(d.items())

s = 'mississippi'
d = defaultdict(int)
for k in s:
    d[k] += 1

sorted(d.items())


s = [('john', 'calculus', 'A'), ('mary', 'calculus', 'A'), ('mary', 'english', 'B'),
     ('mary', 'algebra', 'A'), ('john', 'algebra', 'B+'), ('john', 'english', 'C')]
d = defaultdict(dict)
for name, course, grade in s:
    d[name].update({course: grade})

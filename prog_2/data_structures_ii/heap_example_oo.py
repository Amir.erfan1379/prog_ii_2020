from dataclasses import dataclass
import heapq


@dataclass
class Job:
    priority: int
    id: int
    name: str

    def __lt__(self, other):
        return self.priority < other.priority


taks = [
    Job(4, 1, 'study ai'),
    Job(2, 2, 'study ml'),
    Job(3, 3, 'study dss')]

# Covert to a heap
heapq.heapify(taks)  # O(n)

# Add element
heapq.heappush(taks, Job(0, 4, 'study prog II'))  # O(log(n))

while True:
    try:
        task = heapq.heappop(taks)
        print(task.name)  # O(1)
    except IndexError:
        print('End')
        break

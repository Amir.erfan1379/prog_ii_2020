from typing import Tuple
import numpy as np


def statistics(v: np.ndarray) -> Tuple[float, float, float, float]:
    return v.mean(), v.std(), max(v), min(v)


r = statistics(np.array([1, 2, 3, 4, 5]))

print(f'Mean: {r[0]}')
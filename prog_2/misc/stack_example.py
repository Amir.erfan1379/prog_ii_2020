CONST = 4

def inner(u: int) -> int:
    y = u ** 2
    return y


def outer(u: int) -> int:
    v = inner(u)
    w = v + 3
    return w


print(outer(CONST))


def f(x):
    return 4 * (x ** 2) + 3


def g(x):
    return 2 * x + 1


def fog(x):
    return f(g(x))


fog(1)
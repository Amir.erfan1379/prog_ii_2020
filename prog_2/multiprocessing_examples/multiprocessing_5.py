# The ProcessPoolExecutor class is an Executor subclass that uses a pool of processes to execute
# calls asynchronously. ProcessPoolExecutor uses the multiprocessing module, which allows it to
# side-step the Global Interpreter Lock but also means that only picklable objects can be
# executed and returned.


import concurrent.futures
import time

start = time.perf_counter()


def do_something(seconds):
    print(f'Sleeping {seconds} second(s)...')
    time.sleep(seconds)
    return f'Done Sleeping...{seconds}'


if __name__ == '__main__':
    # https://docs.python.org/3/library/concurrent.futures.html#concurrent.futures.ProcessPoolExecutor
    with concurrent.futures.ProcessPoolExecutor(max_workers=10) as executor:

        # Encapsulates the asynchronous execution of a callable. Future instances are created by
        # Executor.submit()
        results = [executor.submit(do_something, 1) for _ in range(10)]

        # Returns an iterator over the Future instances (possibly created by different Executor
        # instances) given by fs that yields futures as they complete (finished or cancelled
        # futures).
        for f in concurrent.futures.as_completed(results):
            print(f.result())

    finish = time.perf_counter()

    print(f'Finished in {round(finish-start, 2)} second(s)')

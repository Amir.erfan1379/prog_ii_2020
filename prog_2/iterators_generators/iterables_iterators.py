# Adapted from: https://treyhunner.com/2018/02/python-range-is-not-an-iterator/

# In Python an iterable is anything that you can iterate over and an iterator is the thing
# that does the actual iterating.

# Iterator: agents that perform the iteration
# Iterable: objects over which you can iterate over

# You can get an iterator from any iterable in Python by using the iter function:

my_iterator = iter([1, 2])

my_iterator.__repr__()

next(my_iterator)
next(my_iterator)
next(my_iterator)

# You can interate over an iterator (returns itself)

another_iterator = iter([1, 2, 3])
print([x**2 for x in another_iterator])

# iterators are stateful. Meaning once you’ve consumed an item from an iterator, it’s gone.

# iterators are lazy single-use iterables. They’re “lazy” because they have the ability to only
# compute items as you loop over them. And they’re “single-use” because once you’ve “consumed”
# an item from an iterator, it’s gone forever.

# The term “exhausted” is often used for an iterator that has been fully consumed.


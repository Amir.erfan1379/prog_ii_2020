Resumes the execution and sends a value into the generator function.
The value argument becomes the result of the current yield expression.

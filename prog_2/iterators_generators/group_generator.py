from itertools import groupby

data = [("a", 1), ("a", 3), ("b", 0), ("b", 3), ("a", 0)]

def keyfunc(x):
    return x[1]


groups = []
uniquekeys = []
data = sorted(data, key=keyfunc)
for k, g in groupby(data, keyfunc):
    groups.append(list(g))  # Store group iterator as a list
    uniquekeys.append(k)

print(groups)
print(uniquekeys)

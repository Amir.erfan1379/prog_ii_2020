class BasicError(Exception):
    pass


def f(accum=0):

    while True:
        try:
            rcv = yield accum
            if rcv is None:
                accum -= 1
        except BasicError:
            print('Oh, error, reset')
            accum = 0
            yield accum


g = f()
print(next(g))
print(next(g))
g.throw(BasicError)
print(next(g))
print(next(g))
g.close()


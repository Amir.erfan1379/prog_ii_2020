from concurrent.futures import ThreadPoolExecutor
import time


def wait_long():
    print("Sleeping in long", flush=True)
    time.sleep(5)
    return 5


def wait_short():
    print("Sleeping in short", flush=True)
    time.sleep(2)
    return 6


# With context manager
with ThreadPoolExecutor(max_workers=2) as executor:
    a = executor.submit(wait_long)
    b = executor.submit(wait_short)
    c = executor.submit(wait_short)

    print(b.result(), flush=True)
    print(c.result(), flush=True)
    print(a.result(), flush=True)


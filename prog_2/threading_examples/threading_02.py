import time
import threading

start = time.perf_counter()


def do_something(seconds):
    print(f'Sleeping {seconds} second(s)...')
    time.sleep(seconds)
    print(f'Done Sleeping...{seconds}')


if __name__ == '__main__':
    t1 = threading.Thread(target=do_something, args=(1,))
    t2 = threading.Thread(target=do_something, args=(1,))

    t1.start()
    t2.start()

    finish = time.perf_counter()

    print(f'Finished in {round(finish-start, 2)} second(s)')

# Modified from https://www.youtube.com/watch?v=IEEhzQoKtQU
# https://github.com/rvlucerga/code_snippets/blob/master/Python/Threading/threading-demo.py
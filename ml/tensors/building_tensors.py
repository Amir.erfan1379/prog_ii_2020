import torch

# a list

a = [1.0, 2.0, 3.0]

# a tensor from a list

t = torch.tensor([1.0, 2.0, 3.0])

t.shape

t = torch.tensor([[1.0, 2.0, 3.0], [4.0, 5.0, 6.0]])

t.shape

t2 = torch.zeros((3,4))

t2[0,0] = 4.0

# reset points back to original value
points = torch.tensor([[4.0, 1.0], [5.0, 3.0], [2.0, 1.0]])

points[:]
points[1:]
points[1:, :]
points[0, :]
points[1:, 0]

points.dtype

double_points = torch.zeros(10, 2).double()
short_points = torch.ones(10, 2).short()

short_points = torch.tensor([[1, 2], [3, 4]], dtype=torch.short)
short_points = torch.ones(10, 2).to(dtype=torch.short)
double_points = torch.zeros(10, 2).to(torch.double)

# Pointwise

a = torch.randn(2, 1)
b = torch.randn(2, 1)
torch.mul(a, b)



from datetime import datetime

A_CONSTANT = 3


def print_counter():
    counter = 3
    print(datetime.now())
    print(dir())
    print(counter)


print_counter()
print(dir())

a = []
b = []

type(a)

print(a is b)

a = b = []

print(a is b)

a.append(1)
print(a)
print(b)

a = []
print(a is None)
print(not a)

b = []
print(b is None)
print(not b)

a = {}
print(not a)

a.update({'a': 1})
print(not a)

x = None

if x:
  print("Do you think None is True")
else:
  print("None is not True...")

database_connection = None

# Try to connect
try:
    database = MyDatabase(db_host, db_user, db_password, db_database)
    database_connection = database.connect()
except DatabaseException:
    pass

if database_connection is None:
    print('The database could not connect')
else:
    print('The database could connect')


a  = [2, 3]
b = [4, 5]
c = (a, b)

c[0]
type(c[0])
type(c)
id(c[0])
def my_function():
    a_variable = 100
    print(a_variable)


a_variable = 25
my_function()
print(a_variable)

max = 100


def print_max():
    print(max)


print_max()

counter = 100


def print_counter():
    global counter
    counter = counter + 1
    print(counter)


print_counter()


def print_max():
    global counter
    counter = counter + 1
    print(counter)


counter = 100
print_max()
print(counter)


def outer():
    title = "original title"

    def inner():
        nonlocal title
        title = "another title"
        print("inner:", title)

    inner()
    print("outer:", title)


outer()
dir()


def print_max():
    counter = 3
    print(dir())
    print(counter)


print_max()

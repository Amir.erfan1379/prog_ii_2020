def factorial(n: int, depth=1) -> int:
    if n == 1:  # The base case
        return 1
    else:  # The recursive part
        result = n * factorial(n - 1, depth + 1)
        return result


print(factorial(5))

def print_max():
    global counter
    counter = counter + 1
    print(counter)


counter = 100
print_max()
print(counter)


def outer():
    title = "original title"

    def inner():
        nonlocal title
        title = "another title"
        print("inner:", title)

    inner()
    print("outer:", title)


outer()

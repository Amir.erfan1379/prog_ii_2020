UNIT_WEIGHT_PEARS = 0.2
UNIT_WEIGHT_ORANGES = 0.3


class Stock:
    def __init__(self, pears, oranges):
        self.pears = pears
        self.oranges = oranges

    def __add__(self, other):
        total_pears = self.pears + other.pears
        total_oranges = self.oranges + other.oranges
        return Stock(total_pears, total_oranges)

    def __lt__(self, other):
        return ((self.pears * UNIT_WEIGHT_PEARS + self.oranges * UNIT_WEIGHT_ORANGES) <
                (other.pears * UNIT_WEIGHT_PEARS + other.oranges * UNIT_WEIGHT_ORANGES))

    def __str__(self):
        return f'Pears: {self.pears} | Oranges: {self.oranges}'


stock_1 = Stock(pears=3, oranges=3)
stock_2 = Stock(pears=5, oranges=2)

print(stock_1+stock_2)

print(stock_1 < stock_2)
# Python program to check for the target_sum condition to be satisified
# This code is contributed by __Devesh Agrawal__

def objective_sum(a, arr_size, target_sum):
    # sort the array
    quick_sort(a, 0, arr_size - 1)
    l = 0
    r = arr_size - 1

    # traverse the array for the two elements
    while l < r:
        if a[l] + a[r] == target_sum:
            return 1
        elif a[l] + a[r] < target_sum:
            l += 1
        else:
            r -= 1
    return 0


# Implementation of Quick Sort
# a[] --> Array to be sorted
# si --> Starting index
# ei --> Ending index
def quick_sort(a, si, ei):
    if si < ei:
        pi = partition(a, si, ei)
        quick_sort(a, si, pi - 1)
        quick_sort(a, pi + 1, ei)

    # Utility function for partitioning the array(used in quick sort)


def partition(a, si, ei):
    x = a[ei]
    i = (si - 1)
    for j in range(si, ei):
        if a[j] <= x:
            i += 1

            # This operation is used to swap two variables is python
            a[i], a[j] = a[j], a[i]

        a[i + 1], a[ei] = a[ei], a[i + 1]

    return i + 1


def check_objective_sum(a, n):
    # Driver program to test the functions
    if objective_sum(a, len(a), n):
        print("Array has two elements with the given target_sum")
    else:
        print("Array doesn't have two elements with the given target_sum")


if __name__ == '__main__':
    check_objective_sum([1, 4, 45, 6, 10, -8], 16)

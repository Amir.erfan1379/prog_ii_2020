def check(year, month, day, hour, minute, second):
    if 1900 < year < 2100 and 1 <= month <= 12 \
            and 1 <= day <= 31 and 0 <= hour < 24 \
            and 0 <= minute < 60 and 0 <= second < 60:  # Looks like a valid date
        return 1


month_names = ['Januari', 'Februari', 'Maart',      # These are the
               'April', 'Mei', 'Juni',              # Dutch names
               'Juli', 'Augustus', 'September',     # for the months
               'Oktober', 'November', 'December']   # of the year

"""
Multiple indentation mistakes, do not run
"""

"""
 def perm(l):                       # error: first line indented
for i in range(len(l)):             # error: not indented
    s = l[:i] + l[i+1:]
        p = perm(l[:i] + l[i+1:])   # error: unexpected indent
        for x in p:
                r.append(l[i:i+1] + x)
            return r                # error: inconsistent dedent
"""

l = list('abc')


def perm(l):
    # Compute the list of all permutations of l
    if len(l) <= 1:
        return [l]
    r = []
    for i in range(len(l)):
        s = l[:i] + l[i + 1:]
        p = perm(s)
        for x in p:
            r.append(l[i:i + 1] + x)
    return r

print(perm(l))

b'a'

name = "Fred"
f"He said his name is {name!r}."
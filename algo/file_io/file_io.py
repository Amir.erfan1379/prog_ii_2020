# Repository root:
import os
import json

PATH = 'algo/keyboard_screen'
os.chdir(PATH)

with open('workfile.txt') as f:
    read_data = f.read()

# We can check that the file has been automatically closed.
print(f.closed)

with open('workfile.txt') as f:
    print(f.read()) # Reads all
    print(f.read()) # Nothing else to read

with open('workfile.txt') as f:
    print(f.readline()) # Reads first line
    print(f.readline()) # Reads second line

with open('workfile.txt') as f:
    for line in f:
        print(line, end='')

with open('writing.txt', 'w') as f:
    f.write('This is a test\n')


value = ('the answer', 42)
s = str(value)  # convert the tuple to string

with open('writing.txt', 'w') as f:
    f.write(s)

f = open('workfile_binary', 'rb+')
f.write(b'0123456789abcdef')

f.seek(5)      # Go to the 6th byte in the file
f.read(1)
f.seek(-3, 2)  # Go to the 3rd byte before the end
f.read(1)

dic = {"roberto": [9, 8],
       "manuel": [10, 3]}

with open('file.json', 'w') as f:
    json.dump(dic,f, indent=4)

with open('file.json', 'r') as f:
    imported_dic = json.load(f)
    print(imported_dic)